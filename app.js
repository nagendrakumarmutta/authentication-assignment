const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require('dotenv').config()
const data = []

const app = express();

app.listen(5001, ()=>{
    console.log(`Server running at localhost:5001`)
})

app.use(express.json())

let refreshTokens = []
app.post("/token", (req, res) => {
    const refreshToken = req.body.token;
    if(refreshToken === null) return res.sendStatus(401)
    if(!refreshTokens.includes(refreshToken)) return res.sendStatus(403)
    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (error, user) => {
        if(error) return res.sendStatus(403)
        const accessToken = generateAccessToken({name: user.name})
        res.send({accessToken,})
    })

})

app.delete('/logout', (req, res) => {
    refreshTokens = refreshTokens.filter(token => token !== req.body.token)
    res.send("refreshToken deleted successfully")
})

app.post('/api/register', async(req, res) => {
    const {email, username, password} = req.body

    const isValidUsername = (username) => {
        const regex = /^[a-zA-Z ]{2,30}$/;
        return regex.test(username)
    }
    const isValidEmail = (email) => {
        const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
        return regex.test(email)
    }
    const isValidPassword = (password) => {
        return  ((password.length >= 6) && (password.length <= 16)) 
    }
    

    if (!isValidUsername(username)) {
        res.status(400).json({msg:`USERNAME: ${username} is not valid.`})
    } else if (!isValidEmail(email)) {
        res.status(400).json({msg:`EMAIL: ${email} is not valid.`})
    } else if (!isValidPassword(password)) {
        res.status(400).json({msg:"password length should lie between 6 to 16"})
    }else{

    const hashedPassword = await bcrypt.hash(password, 10);

    const foundUser = data.find(eachItem => eachItem.username === username)

    if(!foundUser){
        data.push({email, username, hashedPassword})
        res.status(200).send("User Registered SuccessFully")
    }else{
        res.status(400).send("User already exists")
    }
}
})

app.post('/api/login', async(req, res) => {
    const {username, password} = req.body;
    const foundUser = data.find(eachItem => eachItem.username === username)
    
    if(foundUser){
        const {hashedPassword} = foundUser
        const result =  await bcrypt.compare(password, hashedPassword);

        if(result){
            const payload = {
                username,
            }

            const accessToken = generateAccessToken(payload)
            const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET)
            refreshTokens.push(refreshToken)

            res.send({accessToken, refreshToken})
        }else{
            res.send("password is incorrect")
        }
    }else{
        res.send("user is not registered")
    }
     
})

function generateAccessToken(payload){
      return jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn: "30s"})
}

const authenticateToken = (req, res, next) => {
    let authHeader = req.headers["authorization"]

    if(authHeader !== undefined){
       let jwtToken = authHeader.split(" ")[1]
       jwt.verify(jwtToken, process.env.ACCESS_TOKEN_SECRET, async(error) => {
          if(error){
              res.send("Invalid access token")
          }else{
            next()
        }
        })
} } 


app.patch("/api/reset-password",authenticateToken, async(req, res) => {
    const {username, oldPassword, newPassword} = req.body;
                
    const foundUser = data.find(eachItem => eachItem.username === username);

    if(foundUser){
    const {hashedPassword} = foundUser
    const isPasswordMatched = await bcrypt.compare(oldPassword, hashedPassword)

        if(isPasswordMatched){
            const newHashedPassword = await bcrypt.hash(newPassword, 10);
            data.map(eachItem => {
                if(eachItem.username === username){
                    eachItem["hashedPassword"] = newHashedPassword
                }
            }) 
            
            res.send("Password Updated Successfully")
        }else{
            res.send("Password is incorrect") 
        }
    }else{
    res.send("user is inValid")
    }
    })